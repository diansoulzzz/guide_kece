<?php

use App\Http\Controllers\Admin\Dashboard\ADashboardController;
use App\Http\Controllers\Admin\Master\APackageController;
use App\Http\Controllers\Admin\Master\ATourController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Front\Home\FHomeController;
use App\Http\Controllers\Front\Tour\FTourController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['web', 'revalidate']], function () {
    Route::get('/', [FHomeController::class, 'index'])->name('home');
    Route::group(['prefix' => 'tour'], function () {
        Route::post('checkout', [FTourController::class, 'checkout'])->name('checkout');
        Route::post('payment', [FTourController::class, 'payment'])->name('payment');
        Route::get('confirmation', [FTourController::class, 'confirmation'])->name('confirmation');

        Route::get('/', [FTourController::class, 'index'])->name('tour');
        Route::get('{tour}', [FTourController::class, 'indexDetail'])->name('tour.detail');
    });
    Route::group(['prefix' => 'login'], function () {
        Route::get('/', [LoginController::class, 'index'])->name('login');
        Route::post('/', [LoginController::class, 'authenticate']);
    });
    Route::get('logout', [LoginController::class, 'logout'])->name('logout');
    Route::group(['prefix' => 'register'], function () {
        Route::get('/', [RegisterController::class, 'index'])->name('register');
        Route::post('/', [RegisterController::class, 'register']);
    });
    Route::group(['middleware' => ['auth']], function () {
//        Route::get('history', 'Admin\Dashboard\ADashboardController@index')->name('admin.home');
    });
    Route::group(['middleware' => ['auth:admin']], function () {
        Route::group(['prefix' => 'admin'], function () {
            Route::get('/', [ADashboardController::class, 'index'])->name('admin.home');
            Route::group(['prefix' => 'tour'], function () {
                Route::get('list', [ATourController::class, 'indexList'])->name('admin.tour.list');
                Route::get('del', [ATourController::class, 'deleteData'])->name('admin.tour.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', [ATourController::class, 'indexDetail'])->name('admin.tour.detail');
                    Route::post('/', [ATourController::class, 'postDetail']);
                });
            });
            Route::group(['prefix' => 'package'], function () {
                Route::get('list', [APackageController::class, 'indexList'])->name('admin.package.list');
                Route::get('del', [APackageController::class, 'deleteData'])->name('admin.package.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', [APackageController::class, 'indexDetail'])->name('admin.package.detail');
                    Route::post('/', [APackageController::class, 'postDetail']);
                });
            });

        });
    });
});
