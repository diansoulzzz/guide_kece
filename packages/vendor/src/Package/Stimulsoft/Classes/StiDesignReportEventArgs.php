<?php

namespace Vendor\Package\Stimulsoft\Classes;

class StiDesignReportEventArgs
{
    public $fileName = null;

    function __construct($fileName)
    {
        $this->fileName = $fileName;
    }
}
