<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Tour
 * 
 * @property int $id
 * @property string|null $name
 * @property string|null $description
 * @property float|null $latitude
 * @property float|null $longitude
 * @property float|null $price
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property Collection|TourImg[] $tour_imgs
 * @property Collection|TourPackage[] $tour_packages
 *
 * @package App\Models
 */
class Tour extends Model
{
	use SoftDeletes;
	protected $table = 'tour';

	protected $casts = [
		'latitude' => 'float',
		'longitude' => 'float',
		'price' => 'float'
	];

	protected $fillable = [
		'name',
		'description',
		'latitude',
		'longitude',
		'price'
	];

	public function tour_imgs()
	{
		return $this->hasMany(TourImg::class);
	}

	public function tour_packages()
	{
		return $this->hasMany(TourPackage::class);
	}
}
