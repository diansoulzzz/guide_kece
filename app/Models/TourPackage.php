<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TourPackage
 * 
 * @property int $id
 * @property string|null $name
 * @property int|null $person
 * @property float|null $price_net
 * @property int $food_id
 * @property int $hotel_id
 * @property int $vehicle_id
 * @property int $tour_id
 * 
 * @property Food $food
 * @property Hotel $hotel
 * @property Tour $tour
 * @property Vehicle $vehicle
 * @property Collection|Booking[] $bookings
 *
 * @package App\Models
 */
class TourPackage extends Model
{
	protected $table = 'tour_package';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'person' => 'int',
		'price_net' => 'float',
		'food_id' => 'int',
		'hotel_id' => 'int',
		'vehicle_id' => 'int',
		'tour_id' => 'int'
	];

	protected $fillable = [
		'name',
		'person',
		'price_net',
		'food_id',
		'hotel_id',
		'vehicle_id',
		'tour_id'
	];

	public function food()
	{
		return $this->belongsTo(Food::class);
	}

	public function hotel()
	{
		return $this->belongsTo(Hotel::class);
	}

	public function tour()
	{
		return $this->belongsTo(Tour::class);
	}

	public function vehicle()
	{
		return $this->belongsTo(Vehicle::class);
	}

	public function bookings()
	{
		return $this->hasMany(Booking::class);
	}
}
