<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TourImg
 * 
 * @property int $id
 * @property string $img_url
 * @property string $primary
 * @property int $tour_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property Tour $tour
 *
 * @package App\Models
 */
class TourImg extends Model
{
	use SoftDeletes;
	protected $table = 'tour_img';

	protected $casts = [
		'tour_id' => 'int'
	];

	protected $fillable = [
		'img_url',
		'primary',
		'tour_id'
	];

	public function tour()
	{
		return $this->belongsTo(Tour::class);
	}
}
