<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Booking
 * 
 * @property int $id
 * @property Carbon $tgl
 * @property string $code
 * @property int $tour_package_h_id
 * @property int $users_id
 * @property int $is_paid
 * @property string|null $img
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property int $tour_package_id
 * 
 * @property TourPackage $tour_package
 * @property User $user
 *
 * @package App\Models
 */
class Booking extends Model
{
	use SoftDeletes;
	protected $table = 'booking';

	protected $casts = [
		'tour_package_h_id' => 'int',
		'users_id' => 'int',
		'is_paid' => 'int',
		'tour_package_id' => 'int'
	];

	protected $dates = [
		'tgl'
	];

	protected $fillable = [
		'tgl',
		'code',
		'tour_package_h_id',
		'users_id',
		'is_paid',
		'img',
		'tour_package_id'
	];

	public function tour_package()
	{
		return $this->belongsTo(TourPackage::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class, 'users_id');
	}
}
