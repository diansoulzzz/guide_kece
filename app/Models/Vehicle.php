<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Vehicle
 * 
 * @property int $id
 * @property string $name
 * @property int $capacity
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property Collection|TourPackage[] $tour_packages
 *
 * @package App\Models
 */
class Vehicle extends Model
{
	use SoftDeletes;
	protected $table = 'vehicle';

	protected $casts = [
		'capacity' => 'int'
	];

	protected $fillable = [
		'name',
		'capacity'
	];

	public function tour_packages()
	{
		return $this->hasMany(TourPackage::class);
	}
}
