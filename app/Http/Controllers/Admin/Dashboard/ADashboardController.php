<?php

namespace App\Http\Controllers\Admin\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Company;
use Auth;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Validator;

class ADashboardController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.menus.dashboard.index');
    }
}
