<?php

namespace App\Http\Controllers\Admin\Master;

use App\Constants\Constant;
use App\Helpers\Grid;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Tour;
use App\Traits\FileUploadTraits;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Auth;

class ATourController extends Controller
{
    use FileUploadTraits;

    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = Tour::with(['tour_imgs'])->find($uid);
            return view('admin.menus.master.tour.detail', compact('detail'));
        }
        return view('admin.menus.master.tour.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            $query = 'select * from tour where deleted_at is null';
            $grid = new Grid($request, $query);
            $result = $grid
                ->addAction(route('admin.tour.detail'), 'id', 'la la-edit', '')
                ->addAction(route('admin.tour.delete'), 'id', 'la la-trash', '', 'Yakin Hapus?', 'dialog')
                ->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.master.tour.list');
    }

    public function postDetail(Request $request)
    {
        if ($request->has('xfile')) {
            return $this->uploadFile($request);
        }
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'name' => 'required',
            'fileUrls' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
//        return $inputs->all();
        DB::beginTransaction();
        try {
            $uid = $inputs->input('id');
            $message = 'Tour Berhasil Dibuat';
            $data = new Tour();
            if ($uid) {
                $data = Tour::withTrashed()->find($uid);
                $message = 'Tour Berhasil Diedit';
            }
            $data->name = $request->input('name');
            $data->description = $request->input('description');
            $data->price = $request->input('price');
            $data->latitude = $request->input('latitude');
            $data->longitude = $request->input('longitude');
            $data->save();

            $tourImgs = [];
            $detail = $inputs->input('fileUrls');
            foreach ($detail as $key => $value) {
                $tourImg = [
                    'primary' => 0,
                    'img_url' => $this->moveUploadFile($value, 'tour/' . $data->id . '/'),
                ];
                $tourImgs[] = $tourImg;
            }
            $data->tour_imgs()->createMany($tourImgs);
            DB::commit();
            return Helper::redirect('admin.tour.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function deleteData(Request $request)
    {
        $message = 'Tour Berhasil Dihapus';
        $uid = $request->input('id');
        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }
        $data = Tour::withTrashed()->find($uid);
        $data->delete();
        return Helper::redirect('admin.tour.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }
}
