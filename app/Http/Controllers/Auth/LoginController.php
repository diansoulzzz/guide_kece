<?php

namespace App\Http\Controllers\Auth;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function index(Request $request)
    {
        return view('auth.login');
    }

    public function authenticate(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');
        if (Auth::guard('admin')->attempt(['email' => $username, 'password' => $password, 'is_admin' => 1])) {
            return Helper::redirect('admin.home', Constant::AlertSuccess, Constant::TitleSuccess, 'Welcome Admin');
        }
        if (Auth::attempt(['email' => $username, 'password' => $password])) {
            return Helper::redirect('home', Constant::AlertSuccess, Constant::TitleSuccess, 'Welcome');
        }
        return Helper::redirect('', Constant::AlertDanger, Constant::TitleError, 'Username atau Password salah atau tidak sesuai.');
    }

    public function logout()
    {
        Auth::logout();
        return redirect(route('login'));
    }

    public function injectAdmin()
    {
        $data = new User();
        $email = 'admin@gmail.com';
        $name = 'admin';
        $password = 'admin';
        if ($password) {
            $data->password = Hash::make($password);
        }
        $data->email = $email;
        $data->name = $name;
        $data->is_admin = 1;
        $data->save();
    }
}
