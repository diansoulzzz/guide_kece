<?php

namespace App\Http\Controllers\Auth;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function index(Request $request)
    {
//        return Helper::redirect('home', Constant::AlertSuccess, Constant::TitleSuccess, 'Registrasi berhasil, Mohon lakukan verifikasi.');

        return view('auth.register');
    }

    public function register(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'username' => 'required|email|unique:users,email',
            'name' => 'required',
            'password' => 'required|confirmed',
        ], $message);

        $username = $request->input('username');
        $name = $request->input('name');
        $password = $request->input('password');

        $data = new User();
        if ($password) {
            $data->password = Hash::make($password);
        }
        $data->email = $username;
        $data->name = $name;
        $data->save();
        return Helper::redirect('', Constant::AlertSuccess, Constant::TitleSuccess, 'Registrasi berhasil, Mohon lakukan verifikasi.');
    }

    public function logout()
    {
        Auth::logout();
        return redirect(route('login'));
    }

}
