<?php

namespace App\Http\Controllers\Front\Tour;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Hash;

class FTourController extends Controller
{
    public function index(Request $request)
    {
        return view('front.menus.tour.index');
    }

    public function indexDetail(Request $request)
    {
        return view('front.menus.tour-detail.index');
    }

    public function checkout(Request $request)
    {
        return view('front.menus.checkout.index');
    }

    public function payment(Request $request)
    {
        return redirect(route('confirmation'));
    }

    public function confirmation(Request $request)
    {
        return view('front.menus.confirmation.index');
    }

}
