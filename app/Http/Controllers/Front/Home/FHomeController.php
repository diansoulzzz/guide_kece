<?php

namespace App\Http\Controllers\Front\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Hash;

class FHomeController extends Controller
{
    public function index(Request $request)
    {
        return view('front.menus.home.index');
    }
}
