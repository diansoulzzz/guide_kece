<?php

namespace App\Helpers;

use Illuminate\Http\Request;

class GridHelper
{
    protected $data;
    protected $statement;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function if($field, $condition, $value, callable $element)
    {
        $value = (is_string($value) ? '"' . $value . '"' : $value);
        $field = str_replace(':', '', str_replace('#', '', $field));
        $statement = $this->statement;
        if (!$statement) {
            $statement = "# if (" . $field . $condition . $value . ") { #" . $element($this->data) . " #} #";
        } else {
            $statement = str_replace(' #} #', ' ', $statement);
            $statement .= "#} else if (" . $field . $condition . $value . ") { #" . $element($this->data) . " #} #";
        }
        $this->statement = $statement;
        return $this;
    }

    public function getResult()
    {
        return $this->statement;
    }
}
