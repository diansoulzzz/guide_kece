<?php

namespace App\Traits;

use App\Services\FileUploadProcessor;
use Illuminate\Http\Request;

trait FileUploadTraits
{
    protected $fileUploadProcessor;

    public function __construct(FileUploadProcessor $fileUploadProcessor)
    {
        $this->fileUploadProcessor = $fileUploadProcessor;
    }

    public function uploadFile(Request $request)
    {
        return response([
            'path' => $this->fileUploadProcessor->generateTempFileStoragePath($request)
        ], 200);
    }

    public function moveUploadFile($file, $folder = 'uploads/')
    {
        return $this->fileUploadProcessor->moveFileToRealStoragePath($file, $folder);
    }
}
