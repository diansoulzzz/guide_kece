const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/admin.js', 'public/js')
    .sass('resources/sass/admin.scss', 'public/css')
    .copyDirectory('resources/demo1/src/assets/media', 'public/assets/media')
    .scripts([
        'resources/front/js/jquery-2.2.4.min.js',
        'resources/front/js/common_scripts_min.js',
        'resources/front/js/functions.js',
        'resources/front/js/jquery.sliderPro.min.js',
        // 'resources/front/js/infobox.js'
    ], 'public/js/front.js')
    // .styles([
    //     'resources/front/css/bootstrap.min.css',
    //     'resources/front/css/style.css',
    //     'resources/front/css/vendors.css',
    //     'resources/front/css/custom.css',
    // ], 'public/css/front.css')
    // .script('resources/js/front.js', 'public/js')
    .sass('resources/sass/front.scss', 'public/css')
    .copyDirectory('resources/front/img', 'public/img')
    .copyDirectory('resources/front/css/images', 'public/css/images');

mix.sourceMaps();
mix.webpackConfig({
    resolve: {
        alias: {
            'morris.js': 'morris.js/morris.js',
            'jquery-ui': 'jquery-ui',
        },
    },
});
if (mix.inProduction()) {
    mix.version();
}
