<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{$title="GuideKece | Login"}}</title>
    @include('admin.includes.css')
    <style>
        .kt-login.kt-login--v2 .kt-login__wrapper .kt-login__container .kt-login__logo {
            text-align: center;
            margin: 2rem 0 0 0 !important;
        }

        .kt-login.kt-login--v2 .kt-login__wrapper {
            padding: 13% 2rem 1rem 2rem !important;
            margin: 0 auto 2rem auto;
            overflow: hidden;
        }

        .landing-bg {
            background-image: linear-gradient(rgba(0, 0, 0, 0.2), rgba(255, 255, 255, 0.2)), url('{{asset('images/cover.jpg')}}');
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        .kt-login.kt-login--v2 .kt-login__wrapper {
            padding: 15% 2rem 1rem 2rem;
            margin: 0 auto 2rem auto;
            overflow: hidden;
        }

        .kt-login__signin__shadow {
            padding: 20px;
            border-radius: 25px;
            background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5));
        }

        .kt-login.kt-login--v2 .kt-login__wrapper .kt-login__container .kt-form .form-control {
            height: 46px;
            border-radius: 46px;
            border: none;
            padding-left: 1.5rem;
            padding-right: 1.5rem;
            margin-top: 1.5rem;
            background: rgba(0, 0, 0, 0.4);
            color: #fff;
        }

        .kt-login.kt-login--v2 .kt-login__wrapper .kt-login__container .kt-login__head .kt-login__title {
            text-align: center;
            font-size: 2.2rem;
            font-weight: 500;
            color: #fff;
        }

        .kt-login.kt-login--v2 .kt-login__wrapper .kt-login__container .kt-form {
            margin: 2rem auto;
        }
    </style>
</head>

<body
    class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
<div class="kt-grid kt-grid--ver kt-grid--root">
    <div class="kt-grid kt-grid--hor kt-grid--root kt-login kt-login--v2 kt-login--signin" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor landing-bg">
            <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                <div class="kt-login__container">
                    <div class="kt-login__signin kt-login__signin__shadow">
                        @if (File::exists('images/logo.png'))
                            <div class="kt-login__logo">
                                <img class="landing-logo img-fluid" src="{{asset('images/logo.png')}}" width="250" height="70">
                            </div>
                        @else
                            <div class="kt-login__head">
                                <h3 class="kt-login__title">Please Wait</h3>
                            </div>
                        @endif
                        <div id="login-wait" class="d-flex justify-content-center">
                            <div class="spinner-border text-light" style="width: 3rem; height: 3rem;margin:50px;"
                                 role="status">
                                <span class="sr-only"></span>
                            </div>
                        </div>
                        <form id="form" hidden class="kt-form form-login" method="post" action="{{url()->current()}}"
                              disabled="disabled">
                            @include('admin.includes.alert')
                            @csrf
                            <div class="input-group">
                                <input class="form-control input-white" type="text" placeholder="Username"
                                       name="username"
                                       autocomplete="off">
                            </div>
                            <div class="input-group">
                                <input class="form-control input-white" type="password" placeholder="Password"
                                       name="password">
                            </div>
                            <div class="kt-login__actions">
                                <button id="kt_login_signin_submit" type="submit" disabled
                                        class="btn btn-lg btn-pill btn-skype font-weight-bold">
                                    Sign In
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "dark": "#282a3c",
                "light": "#ffffff",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": [
                    "#c5cbe3",
                    "#a1a8c3",
                    "#3d4465",
                    "#3e4466"
                ],
                "shape": [
                    "#f0f3ff",
                    "#d9dffa",
                    "#afb4d4",
                    "#646c9a"
                ]
            }
        }
    };
</script>
@include('admin.includes.script')
@stack('script')
<script>
    $(function () {
        $('#form').removeAttr('disabled').removeAttr('hidden');
        $('#login-wait').remove();
        $('#kt_login_signin_submit').removeAttr('disabled');
        $('.kt-login__title').html("{{$title}}")
    });
</script>
</body>
</html>
