@extends('admin.layouts.default')
@section('title', $title='Dashboard')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        @include('admin.includes.alert')
    </div>
@endsection
