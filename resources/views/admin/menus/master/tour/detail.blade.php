@extends('admin.layouts.default')
@section('title', $title='Tour')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right form-validatejs" method="post"
                          action="{{url()->current()}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            @isset($detail)
                                <input type="hidden" name="id" value="{{$detail->id}}"/>
                            @endisset
                            <div class="form-group">
                                <label>Name:</label>
                                <input type="text" name="name"
                                       class="form-control" placeholder="Name"
                                       value="{{old('name',(isset($detail)? $detail->name : ''))}}"
                                       required>
                            </div>
                            <div class="form-group">
                                <label>Price:</label>
                                <input type="text"
                                       name="price"
                                       value="{{old('price',(isset($detail)? $detail->price : ''))}}"
                                       class="form-control price input-currency" required>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Latitude:</label>
                                        <input type="text" name="latitude"
                                               class="form-control" placeholder="Latitude"
                                               value="{{old('latitude',(isset($detail)? $detail->latitude : ''))}}"
                                               required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Longitude:</label>
                                        <input type="text" name="longitude"
                                               class="form-control" placeholder="Longitude"
                                               value="{{old('longitude',(isset($detail)? $detail->longitude : ''))}}"
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Description:</label>
                                <textarea type="text"
                                          class="form-control" name="description"
                                          rows="10"
                                          placeholder="Description">{{old('description',(isset($detail)? $detail->description : ''))}}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Image:</label>
                                <input type="hidden" id="fileUrls" name="fileUrls"/>
                                <div class="kt-uppy" id="upload-dashboard">
                                    <div class="kt-uppy__dashboard"></div>
                                    <div class="kt-uppy__progress"></div>
                                </div>
                            </div>
                            @isset($detail)
                                @if($detail->tour_imgs)
                                    <div class="row">
                                        @foreach($detail->tour_imgs as $foto)
                                            <div class="col-3">
                                                <img src="{{Storage::url($foto->img_url)}}" alt="..."
                                                     class="img-thumbnail">
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                            @endisset
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-12 ml-lg-auto">
                                        <button type="submit" class="btn btn-brand">Submit</button>
                                        <a data-url="{{url()->previous()}}"
                                           class="btn btn-secondary prevent-dialog"
                                           data-sw-title="Yakin Cancel?">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('css')
    <style>
        .uppy-size--xl .uppy-Dashboard-Item-preview {
            height: 80px !important;
            width: 80px !important;
        }

        .uppy-size--xl .uppy-Dashboard-Item {
            height: 80px !important;
            width: 80px !important;
        }
    </style>
@endpush
@push('script')
    {{--        <script--}}
    {{--            src='http://maps.google.com/maps/api/js?sensor=false&key=AIzaSyBWjrjOcezSvm79XsrB74tuTjkz9CM6Y10&libraries=places'--}}
    {{--        ></script>--}}
    {{--        <script src="{{asset('plugin/jquery-locationpicker/dist/locationpicker.jquery.min.js')}}"></script>--}}
    <script>
        var fileUrls = [];
        $(function () {
            curForm.init();
        }), curForm = {
            init: function () {
                curForm.initUppy();
                // $('#us5').locationpicker({
                //     location: {
                //         latitude: 42.00,
                //         longitude: -73.82480799999996
                //     },
                //     radius: 300,
                //     onchanged: function (currentLocation, radius, isMarkerDropped) {
                //         var addressComponents = $(this).locationpicker('map').location.addressComponents;
                //         updateControls(addressComponents);
                //     },
                //     oninitialized: function (component) {
                //         var addressComponents = $(component).locationpicker('map').location.addressComponents;
                //         updateControls(addressComponents);
                //     }
                // });
            },
            initUppy: function () {
                var id = '#upload-dashboard';
                var coreOptions = {
                    autoProceed: true,
                    restrictions: {
                        maxFileSize: 1000000,
                        maxNumberOfFiles: 5,
                        minNumberOfFiles: 1,
                        allowedFileTypes: ['image/*', 'video/*']
                    }
                };
                var dashboardOptions = {
                    proudlyDisplayPoweredByUppy: false,
                    target: id,
                    inline: true,
                    replaceTargetContent: true,
                    showProgressDetails: true,
                    note: 'Images and video only, 2–3 files, up to 1 MB',
                    width: '100%',
                    height: 200,
                    metaFields: [
                        {id: 'name', name: 'Name', placeholder: 'file name'},
                        {id: 'caption', name: 'Caption', placeholder: 'describe what the image is about'}
                    ],
                    browserBackButtonClose: true,
                };
                var uppy = Uppy.Core(coreOptions)
                    .use(Uppy.Dashboard, dashboardOptions)
                    .use(Uppy.XHRUpload, {
                        limit: 10,
                        endpoint: '{{url()->current()}}',
                        formData: true,
                        fieldName: 'xfile',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    });
                uppy.on('upload-progress', (file, progress) => {
                    $('button:submit').attr("disabled", "disabled");
                })
                uppy.on('complete', (result) => {
                    var xfileUrls = $.map(result.successful, function (o) {
                        return o['response']['body']['path'];
                    });
                    console.log(xfileUrls);
                    $.merge(fileUrls, xfileUrls)
                    $('#fileUrls').val(JSON.stringify(fileUrls));
                    $('button:submit').removeAttr("disabled");
                });
            },
        }
    </script>
@endpush
