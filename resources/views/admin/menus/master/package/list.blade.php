@extends('admin.layouts.default')
@section('title', $title='Package List')
@push('subheader::menu')
    <div class="d-flex align-items-center">
        <a href="{{route('admin.package.detail')}}"
           class="btn btn-success btn-elevate btn-icon-sm">
            <i class="la la-plus"></i>
            Buat Baru
        </a>
    </div>
@endpush
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        @include('admin.includes.alert')
        <div class="row">
            <div class="col-lg-12">
                <div id="gridContainer"
                     data-grid-name="{{$title}}"
                     class="data-grid"
                     data-grid-url="{{url()->current()}}"></div>
            </div>
        </div>
    </div>
@endsection
