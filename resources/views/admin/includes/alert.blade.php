@if(session('status'))
    <div class="alert {{session('status')['alert']}} fade show" role="alert">
        <div class="alert-text">
            <p>
                <strong>{{session('status')['status']}}</strong>{{session('status')['message']}}
            </p>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="la la-close"></i></span>
            </button>
        </div>
    </div>
@endif
@if ($errors->any())
    <div class="alert alert-danger fade show" role="alert">
        <div class="alert-text">
            <p><strong>Error! </strong>There are some problems.</p>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="la la-close"></i></span>
            </button>
        </div>
    </div>
@endif
