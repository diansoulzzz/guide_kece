<meta name="description" content="Form controls validation">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="csrf-token" content="{{ csrf_token() }}"/>
<link rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
<link href="{{ mix('css/admin.css') }}" rel="stylesheet" type="text/css"/>
<link rel="apple-touch-icon" sizes="57x57" href="{{asset('images/apple-icon-57x57.png')}}">
<link rel="apple-touch-icon" sizes="60x60" href="{{asset('images/apple-icon-60x60.png')}}">
<link rel="apple-touch-icon" sizes="72x72" href="{{asset('images/apple-icon-72x72.png')}}">
<link rel="apple-touch-icon" sizes="76x76" href="{{asset('images/apple-icon-76x76.png')}}">
<link rel="apple-touch-icon" sizes="114x114" href="{{asset('images/apple-icon-114x114.png')}}">
<link rel="apple-touch-icon" sizes="120x120" href="{{asset('images/apple-icon-120x120.png')}}">
<link rel="apple-touch-icon" sizes="144x144" href="{{asset('images/apple-icon-144x144.png')}}">
<link rel="apple-touch-icon" sizes="152x152" href="{{asset('images/apple-icon-152x152.png')}}">
<link rel="apple-touch-icon" sizes="180x180" href="{{asset('images/apple-icon-180x180.png')}}">
<link rel="icon" type="image/png" sizes="192x192" href="{{asset('images/android-icon-192x192.png')}}">
<link rel="icon" type="image/png" sizes="32x32" href="{{asset('images/favicon-32x32.png')}}">
<link rel="icon" type="image/png" sizes="96x96" href="{{asset('images/favicon-96x96.png')}}">
<link rel="icon" type="image/png" sizes="16x16" href="{{asset('images/favicon-16x16.png')}}">
<style>
    .d-none {
        display: none;
    }

    tr.dtrg-group {
        background: #d3d3d3;
    }

    .sidebar-logo {
        width: 150px;
        height: 40px;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 1.5 !important;
    }

    .select2-container--default .select2-selection--single {
        border: 1px solid #e2e5ec !important;
        border-radius: 4px !important;
    }

    .select2-container .select2-selection--single {
        height: auto !important;
    }

    .select2-container *:focus {
        box-shadow: none !important;
        color: #495057 !important;
        background-color: #fff !important;
        border-color: #9aabff !important;
        outline: 0 !important;
    }

    .select2-container--default .select2-selection--single .select2-selection__arrow {
        top: 50% !important;
    }

    .select2-selection__clear {
        top: 43% !important;
    }

    select.form-control {
        border-radius: 25px !important;
    }

    input.form-control:not(.input-white), select.form-control:not(.input-white) {
        color: black;
        font-weight: 300;
    }

    input:focus:not(.input-white), select:focus:not(.input-white) {
        color: black !important;
        font-weight: 400 !important;
    }

    input[readonly], input[disabled], select[disabled] {
        background-color: #eee !important;
    }

    span.select2-selection__rendered {
        color: black !important;
        font-weight: 300;
    }

    .is-invalid .select2-container--default .select2-selection--multiple, .is-invalid .select2-container--default .select2-selection--single {
        border-color: #fd397a !important;
    }

    .is-invalid .select2-container--default .select2-selection--multiple, .is-valid .select2-container--default .select2-selection--single {
        border-color: #0abb87 !important;
    }

    div.k-grid th {
        color: black !important;
    }

    .data-grid {
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif;
        line-height: normal;
    }

    .data-grid .k-grid-header th.k-header:not([data-title="Action"]) {
        background-color: transparent;
        color: #0c0c10 !important;
        min-height: 10px !important;
        padding: 10px;
        font-weight: 600;
        font-size: 14px;
    }

    .data-grid .k-grid-header th.k-header[data-title="Action"] {
        background-color: #313131;
        color: #fff !important;
        min-height: 10px !important;
        padding: 10px;
        font-weight: 600;
        font-size: 15px;
        text-align: center;
    }

    .data-grid .k-grouping-header .k-group-indicator {
        background-color: #007bff;
        color: #fff !important;
        font-weight: 600;
        font-size: 14px;
    }

    .data-grid .k-grid-content td[role="gridcell"] {
        padding: 0 11px;
        color: #000;
        font-weight: 400;
        font-size: 12px;
    }

    .data-grid .k-grid-content td[role="gridcell"] a:not(.btn) {
        color: blue;
    }

    .data-grid .k-grid-content td[role="gridcell"] a.btn {
        color: white;
        padding: 3px;
        margin: auto;
    }

    tr.k-grouping-row td {
        color: #000;
        background-color: #f8f9fa;
        font-weight: 600;
        font-size: 12px;
    }

    tr.k-group-footer td {
        color: #000;
        background-color: #f8f9fa;
        font-weight: 600;
        font-size: 12px;
    }

    tr.k-footer-template td {
        color: #000;
        background-color: #e5eaee;
        font-weight: 800;
        font-size: 12px;
    }

    div.k-grid-norecords {
        width: 100%;
        height: 40px;
        display: flex;
        align-items: center;
    }
</style>
