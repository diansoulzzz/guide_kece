<div class="invisible d-none">
    <ul id="context-menu">
        <li id="ctxtMin">Min</li>
        <li id="ctxtMax">Max</li>
        <li id="ctxtSum">Sum</li>
        <li id="ctxtCount">Count</li>
    </ul>
</div>

<script>
    const KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "dark": "#282a3c",
                "light": "#ffffff",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": [
                    "#c5cbe3",
                    "#a1a8c3",
                    "#3d4465",
                    "#3e4466"
                ],
                "shape": [
                    "#f0f3ff",
                    "#d9dffa",
                    "#afb4d4",
                    "#646c9a"
                ]
            }
        }
    };
</script>
<script src="{{ mix('js/admin.js') }}"></script>
<script>
    $(function () {
        defForm.init();
    }), defForm = {
        init: function () {
            this.ps_init();
            this.ic_init();
            this.pdInit();
            this.fr_init();
            this.s2_init();
            this.da_init();
            this.initGrid();
            $('.form-filter').on('submit', function (e) {
                e.preventDefault();
                $dt.draw();
            });
        },
        ps_init: function () {
            $(".form-validatejs").each(function () {
                $(this).parsley({
                    successClass: "was-validate is-valid",
                    errorClass: "was-validate is-invalid",
                    classHandler: function (el) {
                        if (el.$element.hasClass('kt-select2')) {
                            return el.$element.closest(".form-group");
                        }
                        return el.$element.closest(".form-control");
                    },
                    errorsContainer: function (el) {
                        if (el.$element.parents('div.kt-form__group--inline').length) {
                            return el.$element.closest(".kt-form__group--inline");
                        }
                        return el.$element.closest(".form-group");
                    },
                    errorsWrapper: "<span class='invalid-feedback'></span>",
                    errorTemplate: "<span></span>"
                }).on('form:submit', function (formInstance) {
                    $(".input-currency").each(function () {
                        $(this).inputmask('remove');
                    });
                });
            });
        },
        ic_init: function () {
            $(".input-currency").each(function () {
                $digits = 0;
                if ($(this).data('im-digits') !== undefined) {
                    $digits = $(this).data('im-digits');
                }
                $(this).inputmask('currency', {
                    digits: $digits,
                    autoUnmask: true,
                    prefix: '',
                    rightAlign: false
                });
            });
        },
        pdInit: function () {
            $(".prevent-dialog").each(function () {
                // console.log($(this));
                $title = 'Yakin Hapus?';
                if ($(this).data('sw-title') !== undefined) {
                    $title = $(this).data('sw-title');
                }
                $(this).on('click', function (event) {
                    event.preventDefault();
                    $link = $(this).data('url');
                    swal.fire({
                        title: $title,
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Ya',
                        cancelButtonText: 'Tidak'
                    }).then(function (result) {
                        if (result.value) {
                            location.href = $link;
                        }
                    });
                })
            });
        },
        fr_init: function () {
            $(".form-repeater").each(function () {
                $default = false;
                $undelete_first = true;
                if ($(this).data('default') !== undefined) {
                    $default = $(this).data('default');
                }
                if ($(this).data('repeater-undelete-first') !== undefined) {
                    $undelete_first = $(this).data('repeater-undelete-first');
                }
                // console.log($default);
                $eval = $(this).data('eval');
                $repeater = $(this).repeater({
                    initEmpty: true,
                    isFirstItemUndeletable: $undelete_first,
                    show: function () {
                        $(this).slideDown();
                        defForm.s2_init();
                        defForm.ic_init();
                        eval($eval);
                    },
                    hide: function (deleteElement) {
                        swal.fire({
                            title: 'Yakin Hapus?',
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonText: 'Ya',
                            cancelButtonText: 'Tidak'
                        }).then(function (result) {
                            if (result.value) {
                                $(this).slideUp(deleteElement);
                            }
                        });
                    },
                    afterHide: function (deletedElement) {
                        eval($eval);
                    }
                });
                if ($default) {
                    $repeater.setList($default);
                }
                eval($eval);
            });
        },
        da_init: function () {
            $(".date-picker").each(function () {
                $dateFormat = 'yyyy-mm-dd';
                if ($(this).data('da-locale') !== undefined) {
                    $dateFormat = $(this).data('da-locale');
                }
                $datepicker = $(this).datepicker({
                    rtl: false,
                    autoclose: true,
                    todayHighlight: true,
                    orientation: "bottom left",
                    templates: {
                        leftArrow: '<i class="la la-angle-left"></i>',
                        rightArrow: '<i class="la la-angle-right"></i>'
                    },
                    format: $dateFormat
                });
                $datepicker.inputmask({alias: "datetime", inputFormat: $dateFormat, "clearIncomplete": true});
                if (!$datepicker.val()) {
                    $datepicker.datepicker("setDate", 'now');
                }
            });
            $(".date-range-picker").each(function () {
                $dateFormat = 'yyyy-mm-dd';
                if ($(this).data('da-locale') !== undefined) {
                    $dateFormat = $(this).data('da-locale');
                }
                $(this).daterangepicker({
                    singleDatePicker: false,
                    format: $dateFormat
                });
            });
        },
        s2_init: function () {
            $(".s2-ajax").each(function () {
                $url = $(this).data('s2-url');
                $placeholder = $(this).data('s2-placeholder');
                $selected = false;
                if ($(this).data('s2-selected') !== undefined) {
                    $selected = $(this).data('s2-selected');
                }
                $select2 = $(this).select2({
                    width: '100%',
                    placeholder: $placeholder,
                    allowClear: true,
                    ajax: {
                        url: $url,
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            $active_element = $(this).get(0);
                            $parameters = {
                                q: params.term,
                            };
                            $.map($active_element.attributes, function (v, i) {
                                if (v.name.toLowerCase().indexOf("data-s2-p-") >= 0) {
                                    $name = v.name.toLowerCase().replace("data-s2-p-", "");
                                    $parameters[$name] = v.value;
                                }
                            });
                            return $parameters;
                        },
                        processResults: function (data, params) {
                            params.page = params.page || 1;
                            return {
                                results: data.items,
                                pagination: {
                                    more: (params.page * 30) < data.total_count
                                }
                            };
                        },
                        cache: true
                    },
                    escapeMarkup: function (markup) {
                        return markup;
                    },
                    minimumInputLength: 2,
                    templateResult: defForm.s2FormatTemplateResult,
                    templateSelection: defForm.s2FormatTemplateSelection
                });
                $select2.on("select2:close", function (e) {
                    if ($(this).parents('div.form-group.was-validate').length) {
                        $(this).parsley().validate();
                    }
                });
                if ($(this).data('s2-p-with_all') !== undefined) {
                    if (!$selected) {
                        $sel_text = "ALL";
                        $sel_id = {
                            'id': '%',
                            'text': $sel_text
                        };
                        $selected = {
                            'id': JSON.stringify($sel_id),
                            'text': $sel_text
                        };
                    }
                }
                defForm.s2_set($select2, $selected);
            });
            $(".s2-default").each(function () {
                $placeholder = $(this).data('s2-placeholder');
                $(this).select2({
                    placeholder: $placeholder,
                    allowClear: true,
                });
            });
        },
        s2_set: function ($select2, $selected, $child = '', $param = '') {
            console.log($selected);
            if (!$selected) return;
            $elementName = "id";
            $selectedText = 'text';
            if ($select2.data('s2-selected-text') !== undefined) {
                $selectedText = $select2.data('s2-selected-text');
            }
            $text = '';
            $value = '';
            if ($param) {
                $attr = 'data-s2-p-' + $param;
                $select2.attr($attr, $selected);
            }
            if ($child && $.type($selected) === "string") {
                $selected = JSON.parse($selected)[$child];
            }
            if ($.type($selected) === "string") {
                $value = JSON.parse($selected);
                $text = JSON.parse($value)[$selectedText];
            } else {
                $value = $selected[$elementName];
                $text = $selected[$selectedText];
            }
            $options = new Option($text, $value);
            $select2.empty().append($options).val($value).trigger('change');
        },
        s2FormatTemplateResult: function (data) {
            if (data.loading) return data.text;
            var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository'>";
            // "<div class='select2-result-repository__title'><b>" + data.code + "</b></div>";
            if (data.text) {
                markup += "<div class='select2-result-repository__description'>" + data.text + "</div>";
            }
            markup += "<div class='select2-result-repository__statistics'>" +
                "</div>" +
                "</div></div>";

            return markup;
        },
        s2FormatTemplateSelection: function (data) {
            if (data.id === "") {
                return data.text;
            }
            return data.text;
        },
        dt_createdRow: function (row, data, dataIndex) {
            if (typeof $dt !== 'undefined') {
                $(row).find('td').each(function (index, cell) {
                    $idx = $dt.cell(cell).index().column;
                    $th = $($dt.column($idx).header());
                    if ($th.hasClass('currency')) {
                        $(this).addClass('input-currency');
                    }
                });
            }
        },
        initGrid: function () {
            $(".data-grid").each(function () {
                if ($(this).data('grid-manual') !== undefined) {
                    return false;
                }
                $gridUrl = $(this).data('grid-url');
                $gridName = "grid-options";
                $gridFileName = "";
                if ($(this).data('grid-name') !== undefined) {
                    $gridName = "grid-" + $(this).data('grid-name');
                    $gridFileName = $(this).data('grid-name');
                }
                if ($(this).data('grid-file-name') !== undefined) {
                    $gridFileName = $(this).data('grid-file-name');
                }
                var $dataGridContainer = $(this);
                $.ajax({
                    url: $gridUrl,
                    dataType: "jsonp",
                    success: function (result) {
                        defForm.gridGenerate($dataGridContainer, result.table, $gridName, $gridFileName);
                    }
                });
            });
        },
        gridRefresh: function ($dataGridContainer, gridName) {
            defForm.gridBestFit($dataGridContainer);
            defForm.gridSaveSetting($dataGridContainer);
            defForm.gridMinWidth($dataGridContainer);
            localStorage[gridName] = kendo.stringify($dataGridContainer.getOptions());
        },
        gridGenerate: function ($dataGridContainer, table, gridName, gridFileName) {
            let columns = table['columnDefs'];
            let data = table['rowData'];
            let aggregate = table['rowAggregate'];
            let schema = table['schemaDefs'];
            let group = table['groupDefs'];
            const is_preview = false; //$("input[name='is_preview']").val();
            const options = !is_preview ? localStorage[gridName] : '';

            columns.unshift({
                title: "#",
                field: "row_number_js",
                width: 50,
                aggregates: ["sum", "count", "min", "max"],
                footerTemplate: "",
                groupFooterTemplate: "",
            });

            aggregate.unshift(
                {field: "row_number_js", aggregate: "sum"},
                {field: "row_number_js", aggregate: "min"},
                {field: "row_number_js", aggregate: "max"},
                {field: "row_number_js", aggregate: "count"});

            columns = $.map(columns, function (o, k) {
                if (o["command"] === undefined) {
                    return o;
                }
                var anonFunc = new Function('e', o.command.click);
                o.command.click = anonFunc;
                return o;
            });

            data = $.map(data, function (o, k) {
                o['row_number_js'] = k + 1;
                return o;
            });

            $dataGridContainer.kendoGrid({
                toolbar: ["excel", "pdf", "search", {
                    name: "best_fit",
                    text: "Best Fit All"
                }, {
                    name: "save_grid_setting",
                    text: "Save Grid Setting"
                }],
                columns: columns,
                dataSource: {
                    data: data,
                    schema: schema,
                    group: group,
                    aggregate: aggregate,
                },
                excel: {
                    fileName: gridFileName,
                    filterable: true
                },
                noRecords: {
                    template: "<div class='m-auto'>No records available.</div>",
                },
                // selectable: "multiple",
                filterable: true,
                columnMenu: true,
                sortable: true,
                resizable: true,
                reorderable: true,
                groupable: {
                    showFooter: true
                },
                dataBound: defForm.gridDataBound,
                pageable: {
                    alwaysVisible: false,
                    pageSizes: ['all']
                },
            });
            const grid = $dataGridContainer.data("kendoGrid");
            if (options) {
                $prevOptions = JSON.parse(options);
                $curOptions = grid.getOptions();
                var isColumnSame = true;
                var prevColumnsFields = $.map($prevOptions.columns, function (o) {
                    return (o["field"] || o["title"]) || o["template"];
                });
                var curColumnsFields = $.map($curOptions.columns, function (o) {
                    return (o["field"] || o["title"]) || o["template"];
                });
                // console.log(prevColumnsFields);
                // console.log(curColumnsFields);
                $.each(prevColumnsFields, function (k, v) {
                    if ($.inArray(v, curColumnsFields) < 0) {
                        isColumnSame = false;
                        return false;
                    }
                });
                $.each(curColumnsFields, function (k, v) {
                    if ($.inArray(v, prevColumnsFields) < 0) {
                        isColumnSame = false;
                        return false;
                    }
                });
                delete $prevOptions.dataSource.data;
                delete $prevOptions.dataSource.filter;
                if (!isColumnSame) {
                    delete $prevOptions.columns;
                    delete $prevOptions.dataSource.aggregate;
                    delete $prevOptions.dataSource.fields;
                    delete $prevOptions.dataSource.group;
                    delete $prevOptions.dataSource.schema;
                }
                grid.setOptions({
                    columns: $prevOptions.columns,
                    dataSource: $prevOptions.dataSource,
                });
            }
            window.onbeforeunload = function () {
                localStorage[gridName] = kendo.stringify(grid.getOptions());
            };
            defForm.gridRefresh($dataGridContainer.data("kendoGrid"), gridName);
            defForm.gridCtxMenu($dataGridContainer, gridName);
        },
        gridDataBound: function (e) {
            const grid = this;

            defForm.pdInit();

            grid.thead.find("[role='row'] input.k-checkbox").on("click", function (e) {
                grid.tbody.find("input.k-checkbox").each(function () {
                    const dataItem = grid.dataItem($(this).closest('tr'));
                    $(this).attr("name", 'check_box[]').val(dataItem.id);
                });
            });
            grid.tbody.find("[role='row'] input.k-checkbox").on("click", function (e) {
                console.log(e);
                const dataItem = grid.dataItem($(this).closest('tr'));
                $(this).attr("name", 'check_box[]').val(dataItem.id);
            });
            $(".btn-action").on('click', function () {
                const dataItem = grid.dataItem($(this).closest('tr'));
                $title = $(this).data('title');
                $type = $(this).data('type');
                $fieldName = $(this).data('field-name');
                $link = $(this).data('url');
                if (~$link.indexOf('?')) {
                    $link += "&" + $fieldName + "=" + dataItem[$fieldName];
                } else {
                    $link += "?" + $fieldName + "=" + dataItem[$fieldName];
                }
                if ($type === 'dialog') {
                    swal.fire({
                        title: $title,
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Ya',
                        cancelButtonText: 'Tidak'
                    }).then(function (result) {
                        if (result.value) {
                            location.href = $link;
                        }
                    });
                    return;
                }
                location.href = $link;
            });
        },
        // gridCheckBox: function (e) {
        //     console.log(e);
        //     grid.select(e);
        //
        //     // $(rows).each(function (e) {
        //     //     const row = this;
        //     //     const dataItem = grid.dataItem(row);
        //     //     if (dataItem.granted > 0) {
        //     //         grid.select(row);
        //     //     }
        //     // });
        // },
        gridMinWidth: function (grid) {
            var minTableWidth, minColumnWidth = 100, th, idx;
            grid.resizable.bind("start", function (e) {
                th = $(e.currentTarget).data("th");
                idx = th.index();
            });
            grid.resizable.bind("resize", function (e) {
                if (th.width() >= minColumnWidth) {
                    minTableWidth = grid.tbody.closest("table").width();
                }
                if (th.width() < minColumnWidth) {
                    grid.thead.closest("table").width(minTableWidth).children("colgroup").find("col").eq(idx).width(minColumnWidth);
                    grid.tbody.closest("table").width(minTableWidth).children("colgroup").find("col").eq(idx).width(minColumnWidth);
                }
            });
        },
        gridBestFit: function (grid) {
            $(".k-grid-best_fit").on("click", function (e) {
                for (let i = 0; i < grid.columns.length; i++) {
                    grid.autoFitColumn(i);
                }
            });
        },
        gridSaveSetting: function (grid) {
            $(".k-grid-save_grid_setting").on("click", function (e) {
                console.log(e);
                // for (let i = 0; i < grid.columns.length; i++) {
                //     grid.autoFitColumn(i);
                // }
            });
        },
        gridCtxMenu: function (gridContainer, gridName) {
            $("#context-menu").kendoContextMenu({
                target: gridContainer,
                filter: "td[role='gridcell'][data-field]",
                select: function (e) {
                    var grid = gridContainer.data("kendoGrid");
                    switch (e.item.id) {
                        case "ctxtMin":
                            defForm.gridCtxOnSelected(e, grid, 'Min', 'min', gridName);
                            break;
                        case "ctxtMax":
                            defForm.gridCtxOnSelected(e, grid, 'Max', 'max', gridName);
                            break;
                        case "ctxtSum":
                            defForm.gridCtxOnSelected(e, grid, 'Sum', 'sum', gridName);
                            break;
                        case "ctxtCount":
                            defForm.gridCtxOnSelected(e, grid, 'Count', 'count', gridName);
                            break;
                        default:
                            break;
                    }
                }
            });
        },
        gridCtxOnSelected: function (e, grid, title, type, gridName) {
            var cell = $(e.target);
            var cellName = cell.attr('data-field');
            var selOptions = grid.getOptions();
            $.each(selOptions.columns, function (k, v) {
                var template = title + ": #: " + type + " #";
                if (v.field === cellName) {
                    v.footerTemplate = template;
                    v.footerAttributes = {
                        'class': "table-footer-cell",
                        'style': "text-align: left;"
                    };
                    v.groupFooterTemplate = template;
                }
            });
            grid.setOptions({
                columns: selOptions.columns,
            });
            defForm.gridRefresh(grid, gridName);
        },
    };
</script>
