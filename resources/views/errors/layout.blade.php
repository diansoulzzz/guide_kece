<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>@yield('code', __('Oh no')) </title>
    <meta name="description" content="Login page example">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet" type="text/css"/>
    <style>
        .kt-error-v6 .kt-error_container .kt-error_subtitle > h1 {
            margin-top: 10rem;
        }
    </style>
</head>

<body
    class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
<div class="kt-grid kt-grid--ver kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid  kt-error-v6"
         style="background-image: url({{asset('assets/media/error/bg6.jpg')}});">
        <a href="{{route('home')}}" class="btn btn-link text-light m-2">
            <i class="fas fa-arrow-left"></i> Home
        </a>
        <div class="kt-error_container">
            <div class="kt-error_subtitle kt-font-light">
                <h1>@yield('code', __('Oh no'))</h1>
            </div>
            <p class="kt-error_description kt-font-light">
                Looks like something went wrong.<br>
                @yield('message').
            </p>
        </div>
    </div>
</div>
<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
