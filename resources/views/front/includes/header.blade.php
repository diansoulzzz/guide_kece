<header>
    <div id="top_line">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    @auth
                        <ul id="top_links">
                            <li class="submenu">
                                <a id="cart_items" href="{{url('login')}}">{{Auth::user()->name}}<i
                                        class="icon-down-open-mini"></i></a>
                                <ul class="dropdown-menu" id="cart_items" style="display: none;">
                                    <li>
                                        <div class="image"><img src="img/thumb_cart_1.jpg" alt="image"></div>
                                        <strong><a href="#">Louvre museum</a>1x $36.00 </strong>
                                        <a href="#" class="action"><i class="icon-trash"></i></a>
                                    </li>
                                    <li>
                                        <div class="image"><img src="img/thumb_cart_2.jpg" alt="image"></div>
                                        <strong><a href="#">Versailles tour</a>2x $36.00 </strong>
                                        <a href="#" class="action"><i class="icon-trash"></i></a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    @endauth
                    @guest
                        <ul id="top_links">
                            <li><a href="{{url('login')}}">Login</a></li>
                            <li><a href="{{url('register')}}">Register</a></li>
                        </ul>
                    @endguest
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-3">
                <div id="logo_home">
                    <h1><a href="{{url('/')}}">Home</a></h1>
                </div>
            </div>
            <nav class="col-9">
                <a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a>
                <div class="main-menu">
                    <div id="header_menu">
                        <img src="{{asset('images/logo.png')}}" width="160" height="34" alt="Home"
                             data-retina="true">
                    </div>
                    <a href="#" class="open_close" id="close_in"><i class="icon_set_1_icon-77"></i></a>
                    <ul>
                        <li><a href="{{route('home')}}">Home</a></li>
                        <li><p>|</p></li>
                        <li><a href="{{route('tour')}}">Tour</a></li>
                    </ul>
                </div>
                <ul id="top_tools">
                    <li>
                        <a href="javascript:void(0);" class="search-overlay-menu-btn"><i class="icon_search"></i></a>
                    </li>
                </ul>
            </nav>
        </div>
{{--        <div class="row">--}}
{{--            <div class="col">--}}
{{--                @include('front.includes.alert')--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>
</header>
