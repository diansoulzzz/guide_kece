<footer class="revealed">
    <div class="container">
{{--        <div class="row">--}}
{{--            <div class="col-md-4">--}}
{{--                <h3>Need help?</h3>--}}
{{--                <a href="mailto:help@citytours.com" id="email_footer">help@citytours.com</a>--}}
{{--            </div>--}}
{{--        </div>--}}
        <div class="row">
            <div class="col-md-12">
                <div id="social_footer">
                    <ul>
                        <li><a href="#"><i class="icon-facebook"></i></a></li>
                        <li><a href="#"><i class="icon-twitter"></i></a></li>
                        <li><a href="#"><i class="icon-instagram"></i></a></li>
                    </ul>
                    <p>© {{Constant::AppName}} 2020</p>
                </div>
            </div>
        </div>
    </div>
</footer>
