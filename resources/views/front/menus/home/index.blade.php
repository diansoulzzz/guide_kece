@extends('front.layouts.default')
@section('title', $title='Home')
@section('content')
    <main>
        <div id="carousel-home">
            <div class="owl-carousel owl-theme">
                <div class="owl-slide cover"
                     style="background-image: url({{asset('images/carousel/banyuwangi_lossy.jpg')}});">
                    <div class="opacity-mask d-flex align-items-center" data-opacity-mask="rgba(0, 0, 0, 0.5)">
                        <div class="container">
                            <div class="row justify-content-center justify-content-md-start">
                                <div class="col-lg-12 static">
                                    <div class="slide-text text-center white">
                                        <div class="owl-slide-animated owl-slide-title">
                                            <img class="mx-auto" src="{{asset('images/logo.png')}}"
                                                 style="max-width: 400px !important;">
                                        </div>
                                        <p class="owl-slide-animated owl-slide-subtitle">
                                            <br>
                                            We empower local people who are very familiar with the our destination area.
                                            <br>We are fully prepared to guide you and capture your extraordinary
                                            experience.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="owl-slide cover"
                     style="background-image: url({{asset('images/carousel/pulau-komodo-bg_lossy.jpg')}});">
                    <div class="opacity-mask d-flex align-items-center" data-opacity-mask="rgba(0, 0, 0, 0.6)">
                        <div class="container">
                            <div class="row justify-content-center justify-content-md-end">
                                <div class="col-lg-6 static">
                                    <div class="slide-text text-right white">
                                        <h2 class="owl-slide-animated owl-slide-title">Komodo National Park
                                            <p class="owl-slide-animated owl-slide-subtitle">
                                                Where the Mythical Creature lives
                                                <br>
                                                Meet and greet with the dragons? <br>Why not!
                                            </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="owl-slide cover" style="background-image: url({{asset('images/carousel/sumba-bg.jpg')}});">
                    <div class="opacity-mask d-flex align-items-center" data-opacity-mask="rgba(0, 0, 0, 0.5)">
                        <div class="container">
                            <div class="row justify-content-center justify-content-md-start">
                                <div class="col-lg-6 static">
                                    <div class="slide-text white">
                                        <h2 class="owl-slide-animated owl-slide-title">Sumba
                                        </h2>
                                        <p class="owl-slide-animated owl-slide-subtitle">
                                            Little Heaven in South of Indonesia
                                            <br>
                                            The blend of indigenous culture with the nature makes you don’t want to go
                                            home </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="icon_drag_mobile"></div>
        </div>
        <div class="container margin_60">
            <div class="main_title">
                <h2>Our <span>Top</span> Tours</h2>
                <p>Each item listed with Carousel and Lazy Load Feature.</p>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.1s">
                    <div class="tour_container">
                        <div class="ribbon_3 popular"><span>Popular</span></div>
                        <div class="img_container_2">
                            <div class="owl-carousel owl-theme carousel_item">
                                <div class="item">
                                    <a href="single_tour.html">
                                        <img data-src="img/tour_box_1.jpg" width="800"
                                             height="533"
                                             alt="image" class="img-fluid owl-lazy"></a>
                                </div>
                                <div class="item">
                                    <a href="single_tour.html">
                                        <img data-src="img/tour_box_1_2.jpg" width="800"
                                             height="533"
                                             alt="image" class="img-fluid owl-lazy"></a>
                                </div>
                                <div class="item">
                                    <a href="single_tour.html">
                                        <img data-src="img/tour_box_1_3.jpg" width="800"
                                             height="533"
                                             alt="image" class="img-fluid owl-lazy"></a>
                                </div>
                            </div>
                            <div class="short_info">
                                <i class="icon_set_1_icon-44"></i>Historic Buildings<span
                                    class="price"><sup>$</sup>39</span>
                            </div>
                        </div>
                        <div class="tour_title">
                            <h3><strong>Arc Triomphe</strong> tour</h3>
                            <div class="rating">
                                <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i
                                    class="icon-smile voted"></i><i class="icon-smile voted"></i><i
                                    class="icon-smile"></i><small>(75)</small>
                            </div>
                            <div class="wishlist">
                                <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span
                                        class="tooltip-content-flip"><span
                                            class="tooltip-back">Add to wishlist</span></span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.2s">
                    <div class="tour_container">
                        <div class="ribbon_3 popular"><span>Popular</span></div>
                        <div class="img_container_2">
                            <div class="owl-carousel owl-theme carousel_item">
                                <div class="item">
                                    <a href="single_tour.html">
                                        <img data-src="img/tour_box_2.jpg" width="800"
                                             height="533"
                                             alt="image" class="img-fluid owl-lazy"></a>
                                </div>
                                <div class="item">
                                    <a href="single_tour.html">
                                        <img data-src="img/tour_box_2_2.jpg" width="800"
                                             height="533"
                                             alt="image" class="img-fluid owl-lazy"></a>
                                </div>
                                <div class="item">
                                    <a href="single_tour.html">
                                        <img data-src="img/tour_box_2_3.jpg" width="800"
                                             height="533"
                                             alt="image" class="img-fluid owl-lazy"></a>
                                </div>
                            </div>
                            <div class="short_info">
                                <i class="icon_set_1_icon-43"></i>Churches<span class="price"><sup>$</sup>45</span>
                            </div>
                        </div>
                        <div class="tour_title">
                            <h3><strong>Notredame</strong> tour</h3>
                            <div class="rating">
                                <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i
                                    class="icon-smile voted"></i><i class="icon-smile voted"></i><i
                                    class="icon-smile"></i><small>(75)</small>
                            </div>
                            <div class="wishlist">
                                <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span
                                        class="tooltip-content-flip"><span
                                            class="tooltip-back">Add to wishlist</span></span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.3s">
                    <div class="tour_container">
                        <div class="ribbon_3 popular"><span>Popular</span></div>
                        <div class="img_container_2">
                            <div class="owl-carousel owl-theme carousel_item">
                                <div class="item">
                                    <a href="single_tour.html">
                                        <img data-src="img/tour_box_3.jpg" width="800"
                                             height="533"
                                             alt="image" class="img-fluid owl-lazy"></a>
                                </div>
                                <div class="item">
                                    <a href="single_tour.html">
                                        <img data-src="img/tour_box_3_2.jpg" width="800"
                                             height="533"
                                             alt="image" class="img-fluid owl-lazy"></a>
                                </div>
                                <div class="item">
                                    <a href="single_tour.html">
                                        <img data-src="img/tour_box_3_3.jpg" width="800"
                                             height="533"
                                             alt="image" class="img-fluid owl-lazy"></a>
                                </div>
                            </div>
                            <div class="short_info">
                                <i class="icon_set_1_icon-44"></i>Historic Buildings<span
                                    class="price"><sup>$</sup>48</span>
                            </div>
                        </div>
                        <div class="tour_title">
                            <h3><strong>Versailles</strong> tour</h3>
                            <div class="rating">
                                <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i
                                    class="icon-smile voted"></i><i class="icon-smile voted"></i><i
                                    class="icon-smile"></i><small>(75)</small>
                            </div>
                            <div class="wishlist">
                                <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span
                                        class="tooltip-content-flip"><span
                                            class="tooltip-back">Add to wishlist</span></span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.4s">
                    <div class="tour_container">
                        <div class="ribbon_3"><span>Top rated</span></div>
                        <div class="img_container_2">
                            <div class="owl-carousel owl-theme carousel_item">
                                <div class="item">
                                    <a href="single_tour.html">
                                        <img data-src="img/tour_box_4.jpg" width="800"
                                             height="533"
                                             alt="image" class="img-fluid owl-lazy"></a>
                                </div>
                                <div class="item">
                                    <a href="single_tour.html">
                                        <img data-src="img/tour_box_4_2.jpg" width="800"
                                             height="533"
                                             alt="image" class="img-fluid owl-lazy"></a>
                                </div>
                                <div class="item">
                                    <a href="single_tour.html">
                                        <img data-src="img/tour_box_4_3.jpg" width="800"
                                             height="533"
                                             alt="image" class="img-fluid owl-lazy"></a>
                                </div>
                            </div>
                            <div class="short_info">
                                <i class="icon_set_1_icon-30"></i>Walking tour<span class="price"><sup>$</sup>36</span>
                            </div>
                        </div>
                        <div class="tour_title">
                            <h3><strong>Pompidue</strong> tour</h3>
                            <div class="rating">
                                <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i
                                    class="icon-smile voted"></i><i class="icon-smile voted"></i><i
                                    class="icon-smile"></i><small>(75)</small>
                            </div>
                            <div class="wishlist">
                                <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span
                                        class="tooltip-content-flip"><span
                                            class="tooltip-back">Add to wishlist</span></span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.5s">
                    <div class="tour_container">
                        <div class="ribbon_3"><span>Top rated</span></div>
                        <div class="img_container_2">
                            <div class="owl-carousel owl-theme carousel_item">
                                <div class="item">
                                    <a href="single_tour.html">
                                        <img data-src="img/tour_box_14.jpg" width="800"
                                             height="533"
                                             alt="image" class="img-fluid owl-lazy"></a>
                                </div>
                                <div class="item">
                                    <a href="single_tour.html">
                                        <img data-src="img/tour_box_14_2.jpg" width="800"
                                             height="533" alt="image" class="img-fluid owl-lazy"></a>
                                </div>
                                <div class="item">
                                    <a href="single_tour.html">
                                        <img data-src="img/tour_box_14_3.jpg" width="800"
                                             height="533" alt="image" class="img-fluid owl-lazy"></a>
                                </div>
                            </div>
                            <div class="short_info">
                                <i class="icon_set_1_icon-28"></i>Skyline tours<span class="price"><sup>$</sup>42</span>
                            </div>
                        </div>
                        <div class="tour_title">
                            <h3><strong>Tour Eiffel</strong> tour</h3>
                            <div class="rating">
                                <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i
                                    class="icon-smile voted"></i><i class="icon-smile voted"></i><i
                                    class="icon-smile"></i><small>(75)</small>
                            </div>
                            <div class="wishlist">
                                <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span
                                        class="tooltip-content-flip"><span
                                            class="tooltip-back">Add to wishlist</span></span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 wow zoomIn" data-wow-delay="0.6s">
                    <div class="tour_container">
                        <div class="ribbon_3"><span>Top rated</span></div>
                        <div class="img_container_2">
                            <div class="owl-carousel owl-theme carousel_item">
                                <div class="item">
                                    <a href="single_tour.html">
                                        <img data-src="img/tour_box_5.jpg" width="800"
                                             height="533"
                                             alt="image" class="img-fluid owl-lazy"></a>
                                </div>
                                <div class="item">
                                    <a href="single_tour.html">
                                        <img data-src="img/tour_box_5_2.jpg" width="800"
                                             height="533"
                                             alt="image" class="img-fluid owl-lazy"></a>
                                </div>
                                <div class="item">
                                    <a href="single_tour.html">
                                        <img data-src="img/tour_box_5_3.jpg" width="800"
                                             height="533"
                                             alt="image" class="img-fluid owl-lazy"></a>
                                </div>
                            </div>
                            <div class="short_info">
                                <i class="icon_set_1_icon-44"></i>Historic Buildings<span
                                    class="price"><sup>$</sup>40</span>
                            </div>
                        </div>
                        <div class="tour_title">
                            <h3><strong>Pantheon</strong> tour</h3>
                            <div class="rating">
                                <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i
                                    class="icon-smile voted"></i><i class="icon-smile voted"></i><i
                                    class="icon-smile"></i><small>(75)</small>
                            </div>
                            <div class="wishlist">
                                <a class="tooltip_flip tooltip-effect-1" href="javascript:void(0);">+<span
                                        class="tooltip-content-flip"><span
                                            class="tooltip-back">Add to wishlist</span></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-center add_bottom_30">
                <a href="all_tours_list.html" class="btn_1">View all Tours</a>
            </p>
        </div>
        <div class="white_bg">
            <div class="container margin_60">
                <div class="main_title">
                    <h2>Plan <span>Your Tour</span> Easly</h2>
                    <p>
                        Quisque at tortor a libero posuere laoreet vitae sed arcu. Curabitur consequat.
                    </p>
                </div>
                <div class="row feature_home_2">
                    <div class="col-md-4 text-center">
                        <img src="img/adventure_icon_1.svg" alt="" width="75" height="75">
                        <h3>Itineraries studied in detail</h3>
                        <p>Suscipit invenire petentium per in. Ne magna assueverit vel. Vix movet perfecto facilisis in,
                            ius
                            ad maiorum corrumpit, his esse docendi in.</p>
                    </div>
                    <div class="col-md-4 text-center">
                        <img src="img/adventure_icon_2.svg" alt="" width="75" height="75">
                        <h3>Room and food included</h3>
                        <p> Cum accusam voluptatibus at, et eum fuisset sententiae. Postulant tractatos ius an, in vis
                            fabulas percipitur, est audiam phaedrum electram ex.</p>
                    </div>
                    <div class="col-md-4 text-center">
                        <img src="img/adventure_icon_3.svg" alt="" width="75" height="75">
                        <h3>Everything organized</h3>
                        <p>Integre vivendo percipitur eam in, graece suavitate cu vel. Per inani persius accumsan no. An
                            case duis option est, pro ad fastidii contentiones.</p>
                    </div>
                </div>
                <div class="banner_2">
                    <div class="wrapper d-flex align-items-center opacity-mask" data-opacity-mask="rgba(0, 0, 0, 0.3)"
                         style="background-image: url({{asset('images/carousel/padar_island_lossy.jpg')}});
                             background-repeat: no-repeat;
                             background-size: cover;
                             background-position: center;
                             ">
                        <div>
                            <h3>What are you waiting for?</h3>
                            <a href="all_tours_list.html" class="btn_1">Plan your escape now!</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
