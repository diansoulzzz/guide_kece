@extends('front.layouts.default')
@section('title', $title='Home')
@section('content')
    <section class="parallax-window" data-parallax="scroll"
             data-image-src="{{asset('images/carousel/komodo_sailing.jpg')}}"
             data-natural-width="1400" data-natural-height="470">
        <div class="parallax-content-2">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <span class="rating"><i class="icon-star voted"></i><i class="icon-star voted"></i><i
                                class="icon-star voted"></i><i class="icon-star voted"></i><i
                                class=" icon-star-empty"></i></span>
                        <h1>Mariott Hotel</h1>
                        <span>Champ de Mars, 5 Avenue Anatole, 75007 Paris.</span>
                    </div>
                    <div class="col-md-4">
                        <div id="price_single_main" class="hotel">
                            from/per night <span><sup>$</sup>95</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <main>
        <div id="position">
            <div class="container">
                <ul>
                    <li><a href="#">Home</a>
                    </li>
                    <li><a href="#">Category</a>
                    </li>
                    <li>Page active</li>
                </ul>
            </div>
        </div>
        <div class="collapse" id="collapseMap">
            <div id="map" class="map"></div>
        </div>
        <div class="container margin_60">
            <div class="row">
                <div class="col-lg-8" id="single_tour_desc">
                    <div id="single_tour_feat">
                        <ul>
                            <li><i class="icon_set_2_icon-116"></i>Plasma TV</li>
                            <li><i class="icon_set_1_icon-86"></i>Free Wifi</li>
                            <li><i class="icon_set_2_icon-110"></i>Poll</li>
                            <li><i class="icon_set_1_icon-59"></i>Breakfast</li>
                            <li><i class="icon_set_1_icon-22"></i>Pet allowed</li>
                            <li><i class="icon_set_1_icon-13"></i>Accessibiliy</li>
                            <li><i class="icon_set_1_icon-27"></i>Parking</li>
                        </ul>
                    </div>
                    <p class="d-none d-md-block d-block d-lg-none"><a class="btn_map" data-toggle="collapse"
                                                                      href="#collapseMap" aria-expanded="false"
                                                                      aria-controls="collapseMap"
                                                                      data-text-swap="Hide map"
                                                                      data-text-original="View on map">View on map</a>
                    </p>
                    <div id="Img_carousel" class="slider-pro">
                        <div class="sp-slides">

                            <div class="sp-slide">
                                <img alt="Image" class="sp-image" src="{{asset('css/images/blank.gif')}}"
                                     data-src="{{asset('img/slider_single_tour/1_medium.jpg')}}"
                                     data-small="{{asset('img/slider_single_tour/1_small.jpg')}}"
                                     data-medium="{{asset('img/slider_single_tour/1_medium.jpg')}}"
                                     data-large="{{asset('img/slider_single_tour/1_large.jpg')}}"
                                     data-retina="{{asset('img/slider_single_tour/1_large.jpg')}}">
                            </div>
                            <div class="sp-slide">
                                <img alt="Image" class="sp-image" src="{{asset('css/images/blank.gif')}}"
                                     data-src="{{asset('img/slider_single_tour/2_medium.jpg')}}"
                                     data-small="{{asset('img/slider_single_tour/2_small.jpg')}}"
                                     data-medium="{{asset('img/slider_single_tour/2_medium.jpg')}}"
                                     data-large="{{asset('img/slider_single_tour/2_large.jpg')}}"
                                     data-retina="{{asset('img/slider_single_tour/2_large.jpg')}}">
                            </div>

                            <div class="sp-slide">
                                <img alt="Image" class="sp-image" src="{{asset('css/images/blank.gif')}}"
                                     data-src="{{asset('img/slider_single_tour/3_medium.jpg')}}"
                                     data-small="{{asset('img/slider_single_tour/3_small.jpg')}}"
                                     data-medium="{{asset('img/slider_single_tour/3_medium.jpg')}}"
                                     data-large="{{asset('img/slider_single_tour/3_large.jpg')}}"
                                     data-retina="{{asset('img/slider_single_tour/3_large.jpg')}}">
                            </div>

                            <div class="sp-slide">
                                <img alt="Image" class="sp-image" src="{{asset('css/images/blank.gif')}}"
                                     data-src="{{asset('img/slider_single_tour/4_medium.jpg')}}"
                                     data-small="{{asset('img/slider_single_tour/4_small.jpg')}}"
                                     data-medium="{{asset('img/slider_single_tour/4_medium.jpg')}}"
                                     data-large="{{asset('img/slider_single_tour/4_large.jpg')}}"
                                     data-retina="{{asset('img/slider_single_tour/4_large.jpg')}}">
                            </div>

                            <div class="sp-slide">
                                <img alt="Image" class="sp-image" src="{{asset('css/images/blank.gif')}}"
                                     data-src="{{asset('img/slider_single_tour/5_medium.jpg')}}"
                                     data-small="{{asset('img/slider_single_tour/5_small.jpg')}}"
                                     data-medium="{{asset('img/slider_single_tour/5_medium.jpg')}}"
                                     data-large="{{asset('img/slider_single_tour/5_large.jpg')}}"
                                     data-retina="{{asset('img/slider_single_tour/5_large.jpg')}}">
                            </div>

                            <div class="sp-slide">
                                <img alt="Image" class="sp-image" src="{{asset('css/images/blank.gif')}}"
                                     data-src="{{asset('img/slider_single_tour/6_medium.jpg')}}"
                                     data-small="{{asset('img/slider_single_tour/6_small.jpg')}}"
                                     data-medium="{{asset('img/slider_single_tour/6_medium.jpg')}}"
                                     data-large="{{asset('img/slider_single_tour/6_large.jpg')}}"
                                     data-retina="{{asset('img/slider_single_tour/6_large.jpg')}}">
                            </div>

                            <div class="sp-slide">
                                <img alt="Image" class="sp-image" src="{{asset('css/images/blank.gif')}}"
                                     data-src="{{asset('img/slider_single_tour/7_medium.jpg')}}"
                                     data-small="{{asset('img/slider_single_tour/7_small.jpg')}}"
                                     data-medium="{{asset('img/slider_single_tour/7_medium.jpg')}}"
                                     data-large="{{asset('img/slider_single_tour/7_large.jpg')}}"
                                     data-retina="{{asset('img/slider_single_tour/7_large.jpg')}}">
                            </div>

                            <div class="sp-slide">
                                <img alt="Image" class="sp-image" src="{{asset('css/images/blank.gif')}}"
                                     data-src="{{asset('img/slider_single_tour/8_medium.jpg')}}"
                                     data-small="{{asset('img/slider_single_tour/8_small.jpg')}}"
                                     data-medium="{{asset('img/slider_single_tour/8_medium.jpg')}}"
                                     data-large="{{asset('img/slider_single_tour/8_large.jpg')}}"
                                     data-retina="{{asset('img/slider_single_tour/8_large.jpg')}}">
                            </div>

                            <div class="sp-slide">
                                <img alt="Image" class="sp-image" src="{{asset('css/images/blank.gif')}}"
                                     data-src="{{asset('img/slider_single_tour/9_medium.jpg')}}"
                                     data-small="{{asset('img/slider_single_tour/9_small.jpg')}}"
                                     data-medium="{{asset('img/slider_single_tour/9_medium.jpg')}}"
                                     data-large="{{asset('img/slider_single_tour/9_large.jpg')}}"
                                     data-retina="{{asset('img/slider_single_tour/9_large.jpg')}}">
                            </div>
                        </div>
                        <div class="sp-thumbnails">
                            <img alt="Image" class="sp-thumbnail"
                                 src="{{asset('img/slider_single_tour/1_medium.jpg')}}">
                            <img alt="Image" class="sp-thumbnail"
                                 src="{{asset('img/slider_single_tour/2_medium.jpg')}}">
                            <img alt="Image" class="sp-thumbnail"
                                 src="{{asset('img/slider_single_tour/3_medium.jpg')}}">
                            <img alt="Image" class="sp-thumbnail"
                                 src="{{asset('img/slider_single_tour/4_medium.jpg')}}">
                            <img alt="Image" class="sp-thumbnail"
                                 src="{{asset('img/slider_single_tour/5_medium.jpg')}}">
                            <img alt="Image" class="sp-thumbnail"
                                 src="{{asset('img/slider_single_tour/6_medium.jpg')}}">
                            <img alt="Image" class="sp-thumbnail"
                                 src="{{asset('img/slider_single_tour/7_medium.jpg')}}">
                            <img alt="Image" class="sp-thumbnail"
                                 src="{{asset('img/slider_single_tour/8_medium.jpg')}}">
                            <img alt="Image" class="sp-thumbnail"
                                 src="{{asset('img/slider_single_tour/9_medium.jpg')}}">
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-lg-3">
                            <h3>Description</h3>
                        </div>
                        <div class="col-lg-9">
                            <p>
                                Lorem ipsum dolor sit amet, at omnes deseruisse pri. Quo aeterno legimus insolens ad.
                                Sit cu detraxit constituam, an mel iudico constituto efficiendi. Eu ponderum mediocrem
                                has, vitae adolescens in pro. Mea liber ridens inermis ei, mei legendos vulputate an,
                                labitur tibique te qui.
                            </p>
                            <h4>Hotel facilities</h4>
                            <p>
                                Lorem ipsum dolor sit amet, at omnes deseruisse pri. Quo aeterno legimus insolens ad.
                                Sit cu detraxit constituam, an mel iudico constituto efficiendi.
                            </p>
                            <div class="row">
                                <div class="col-md-6">
                                    <ul class="list_ok">
                                        <li>Lorem ipsum dolor sit amet</li>
                                        <li>No scripta electram necessitatibus sit</li>
                                        <li>Quidam percipitur instructior an eum</li>
                                        <li>Ut est saepe munere ceteros</li>
                                        <li>No scripta electram necessitatibus sit</li>
                                        <li>Quidam percipitur instructior an eum</li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <ul class="list_ok">
                                        <li>Lorem ipsum dolor sit amet</li>
                                        <li>No scripta electram necessitatibus sit</li>
                                        <li>Quidam percipitur instructior an eum</li>
                                        <li>No scripta electram necessitatibus sit</li>
                                    </ul>
                                </div>
                            </div>
                            <!-- End row  -->
                        </div>
                        <!-- End col-md-9  -->
                    </div>
                    <!-- End row  -->

                    <hr>

                    <div class="row">
                        <div class="col-lg-3">
                            <h3>Rooms Types</h3>
                        </div>
                        <div class="col-lg-9">
                            <h4>Single Room</h4>
                            <p>
                                Lorem ipsum dolor sit amet, at omnes deseruisse pri. Quo aeterno legimus insolens ad.
                                Sit cu detraxit constituam, an mel iudico constituto efficiendi.
                            </p>

                            <div class="row">
                                <div class="col-md-6">
                                    <ul class="list_icons">
                                        <li><i class="icon_set_1_icon-86"></i> Free wifi</li>
                                        <li><i class="icon_set_2_icon-116"></i> Plasma Tv</li>
                                        <li><i class="icon_set_2_icon-106"></i> Safety box</li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <ul class="list_ok">
                                        <li>Lorem ipsum dolor sit amet</li>
                                        <li>No scripta electram necessitatibus sit</li>
                                        <li>Quidam percipitur instructior an eum</li>
                                    </ul>
                                </div>
                            </div>
                            <!-- End row  -->
                            <div class="owl-carousel owl-theme carousel-thumbs-2 magnific-gallery">
                                <div class="item">
                                    <a href="{{asset('img/carousel/1.jpg')}}" data-effect="mfp-zoom-in"><img
                                            src="{{asset('img/carousel/1.jpg')}}"
                                            alt="Image">
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="{{asset('img/carousel/2.jpg')}}" data-effect="mfp-zoom-in"><img
                                            src="{{asset('img/carousel/2.jpg')}}"
                                            alt="Image">
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="{{asset('img/carousel/3.jpg')}}" data-effect="mfp-zoom-in"><img
                                            src="{{asset('img/carousel/3.jpg')}}"
                                            alt="Image">
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="{{asset('img/carousel/4.jpg')}}" data-effect="mfp-zoom-in"><img
                                            src="{{asset('img/carousel/4.jpg')}}"
                                            alt="Image">
                                    </a>
                                </div>
                            </div>
                            <!-- End photo carousel  -->

                            <hr>

                            <h4>Double Room</h4>
                            <p>
                                Lorem ipsum dolor sit amet, at omnes deseruisse pri. Quo aeterno legimus insolens ad.
                                Sit cu detraxit constituam, an mel iudico constituto efficiendi.
                            </p>

                            <div class="row">
                                <div class="col-md-6">
                                    <ul class="list_icons">
                                        <li><i class="icon_set_1_icon-86"></i> Free wifi</li>
                                        <li><i class="icon_set_2_icon-116"></i> Plasma Tv</li>
                                        <li><i class="icon_set_2_icon-106"></i> Safety box</li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <ul class="list_ok">
                                        <li>Lorem ipsum dolor sit amet</li>
                                        <li>No scripta electram necessitatibus sit</li>
                                        <li>Quidam percipitur instructior an eum</li>
                                    </ul>
                                </div>
                            </div>
                            <!-- End row  -->
                            <div class="owl-carousel owl-theme carousel-thumbs-2 magnific-gallery">
                                <div class="item">
                                    <a href="{{asset('img/carousel/1.jpg')}}" data-effect="mfp-zoom-in"><img
                                            src="{{asset('img/carousel/1.jpg')}}"
                                            alt="Image">
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="{{asset('img/carousel/2.jpg')}}" data-effect="mfp-zoom-in"><img
                                            src="{{asset('img/carousel/2.jpg')}}"
                                            alt="Image">
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="{{asset('img/carousel/3.jpg')}}" data-effect="mfp-zoom-in"><img
                                            src="{{asset('img/carousel/3.jpg')}}"
                                            alt="Image">
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="{{asset('img/carousel/4.jpg')}}" data-effect="mfp-zoom-in"><img
                                            src="{{asset('img/carousel/4.jpg')}}"
                                            alt="Image">
                                    </a>
                                </div>
                            </div>
                            <!-- End photo carousel  -->
                        </div>
                        <!-- End col-md-9  -->
                    </div>
                    <!-- End row  -->

                    <hr>

                    <div class="row">
                        <div class="col-lg-3">
                            <h3>Reviews</h3>
                            <a href="#" class="btn_1 add_bottom_30" data-toggle="modal" data-target="#myReview">Leave a
                                review</a>
                        </div>
                        <div class="col-lg-9">
                            <div id="score_detail"><span>7.5</span>Good <small>(Based on 34 reviews)</small>
                            </div>
                            <!-- End general_rating -->
                            <div class="row" id="rating_summary">
                                <div class="col-md-6">
                                    <ul>
                                        <li>Position
                                            <div class="rating">
                                                <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i
                                                    class="icon-smile voted"></i><i class="icon-smile"></i><i
                                                    class="icon-smile"></i>
                                            </div>
                                        </li>
                                        <li>Comfort
                                            <div class="rating">
                                                <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i
                                                    class="icon-smile voted"></i><i class="icon-smile voted"></i><i
                                                    class="icon-smile"></i>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <ul>
                                        <li>Price
                                            <div class="rating">
                                                <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i
                                                    class="icon-smile voted"></i><i class="icon-smile"></i><i
                                                    class="icon-smile"></i>
                                            </div>
                                        </li>
                                        <li>Quality
                                            <div class="rating">
                                                <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i
                                                    class="icon-smile voted"></i><i class="icon-smile voted"></i><i
                                                    class="icon-smile voted"></i>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <hr>
                            <div class="review_strip_single">
                                <img src="{{asset('img/avatar1.jpg')}}" alt="Image" class="rounded-circle">
                                <small> - 10 March 2015 -</small>
                                <h4>Jhon Doe</h4>
                                <p>
                                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a lorem quis neque
                                    interdum consequat ut sed sem. Duis quis tempor nunc. Interdum et malesuada fames ac
                                    ante ipsum primis in faucibus."
                                </p>
                                <div class="rating">
                                    <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i
                                        class="icon-smile voted"></i><i class="icon-smile"></i><i
                                        class="icon-smile"></i>
                                </div>
                            </div>
                            <!-- End review strip -->

                            <div class="review_strip_single">
                                <img src="{{asset('img/avatar2.jpg')}}" alt="Image" class="rounded-circle">
                                <small> - 10 March 2015 -</small>
                                <h4>Jhon Doe</h4>
                                <p>
                                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a lorem quis neque
                                    interdum consequat ut sed sem. Duis quis tempor nunc. Interdum et malesuada fames ac
                                    ante ipsum primis in faucibus."
                                </p>
                                <div class="rating">
                                    <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i
                                        class="icon-smile voted"></i><i class="icon-smile"></i><i
                                        class="icon-smile"></i>
                                </div>
                            </div>
                            <!-- End review strip -->

                            <div class="review_strip_single last">
                                <img src="{{asset('img/avatar3.jpg')}}" alt="Image" class="rounded-circle">
                                <small> - 10 March 2015 -</small>
                                <h4>Jhon Doe</h4>
                                <p>
                                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a lorem quis neque
                                    interdum consequat ut sed sem. Duis quis tempor nunc. Interdum et malesuada fames ac
                                    ante ipsum primis in faucibus."
                                </p>
                                <div class="rating">
                                    <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i
                                        class="icon-smile voted"></i><i class="icon-smile"></i><i
                                        class="icon-smile"></i>
                                </div>
                            </div>
                            <!-- End review strip -->
                        </div>
                    </div>
                </div>
                <!--End  single_tour_desc-->

                <aside class="col-lg-4">
                    <p class="d-none d-xl-block d-lg-block d-xl-none">
                        <a class="btn_map" data-toggle="collapse" href="#collapseMap" aria-expanded="false"
                           aria-controls="collapseMap" data-text-swap="Hide map" data-text-original="View on map">View
                            on map</a>
                    </p>
                    <form method="post" action="{{route('checkout')}}">
                        @csrf
                        <div class="box_style_1 expose">
                            <h3 class="inner">Check Availability</h3>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label><i class="icon-calendar-7"></i> Check in</label>
                                        <input class="date-pick form-control" data-date-format="M d, D" type="text">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label><i class="icon-calendar-7"></i> Check out</label>
                                        <input class="date-pick form-control" data-date-format="M d, D" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Adults</label>
                                        <div class="numbers-row">
                                            <input type="text" value="1" id="adults" class="qty2 form-control"
                                                   name="quantity">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Children</label>
                                        <div class="numbers-row">
                                            <input type="text" value="0" id="children" class="qty2 form-control"
                                                   name="quantity">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <button class="btn_full" type="submit">Order Now</button>
                            {{--                            <a class="btn_full" href="cart_hotel.html">Order now</a>--}}
                            {{--                                                    <a class="btn_full_outline" href="#"><i class=" icon-heart"></i> Add to whislist</a>--}}
                        </div>
                    </form>
                    <div class="box_style_4">
                        <i class="icon_set_1_icon-90"></i>
                        <h4><span>Book</span> by phone</h4>
                        <div class="row justify-content-md-center mt-3">
                            <div class="col-4">
                                <a href="tel://11111" class="phone">
                                    <img class="img-thumbnail" style="max-width: 50px;"
                                         src="{{asset('images/icon-whatsapp.png')}}">
                                </a>
                            </div>
                            <div class="col-4">
                                <a href="tel://11111" class="phone">
                                    <img class="img-thumbnail" style="max-width: 50px;"
                                         src="{{asset('images/icon-gmail.png')}}">
                                </a>
                            </div>
                        </div>
                        <small>Monday to Friday 9.00am - 7.30pm</small>
                    </div>

                </aside>
            </div>
        </div>
        <div id="overlay"></div>
    </main>
@endsection

@push('script')
    <script>
        $(function () {
            curForm.init();
        }), curForm = {
            init: function () {
                $('#Img_carousel').sliderPro({
                    width: 960,
                    height: 500,
                    fade: true,
                    arrows: true,
                    buttons: false,
                    fullScreen: false,
                    smallSize: 500,
                    startSlide: 0,
                    mediumSize: 1000,
                    largeSize: 3000,
                    thumbnailArrows: true,
                    autoplay: false
                });
                $('.carousel-thumbs-2').owlCarousel({
                    loop: false,
                    margin: 5,
                    responsiveClass: true,
                    nav: false,
                    responsive: {
                        0: {
                            items: 1
                        },
                        600: {
                            items: 3
                        },
                        1000: {
                            items: 4,
                            nav: false
                        }
                    }
                });
            }
        }
    </script>
@endpush
