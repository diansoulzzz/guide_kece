@extends('front.layouts.default')
@section('title', $title='Home')
@section('content')
    <section class="parallax-window" data-parallax="scroll"
             data-image-src="{{asset('images/carousel/komodo_sailing.jpg')}}" data-natural-width="1400"
             data-natural-height="470">
        <div class="parallax-content-1">
            <div class="animated fadeInDown">
                <h1>Checkout</h1>
                <div class="bs-wizard row">
                    <div class="col-4 bs-wizard-step complete">
                        <div class="text-center bs-wizard-stepnum">Your order</div>
                        <div class="progress">
                            <div class="progress-bar"></div>
                        </div>
                        <a href="cart_hotel.html" class="bs-wizard-dot"></a>
                    </div>
                    <div class="col-4 bs-wizard-step active">
                        <div class="text-center bs-wizard-stepnum">Pick Location & Payment</div>
                        <div class="progress">
                            <div class="progress-bar"></div>
                        </div>
                        <a href="#" class="bs-wizard-dot"></a>
                    </div>

                    <div class="col-4 bs-wizard-step disabled">
                        <div class="text-center bs-wizard-stepnum">Finish!</div>
                        <div class="progress">
                            <div class="progress-bar"></div>
                        </div>
                        <a href="confirmation_hotel.html" class="bs-wizard-dot"></a>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <main>
        <div id="position">
            <div class="container">
                <ul>
                    <li><a href="#">Home</a>
                    </li>
                    <li><a href="#">Category</a>
                    </li>
                    <li>Page active</li>
                </ul>
            </div>
        </div>
        <!-- End position -->

        <div class="container margin_60">
            <form method="post" action="{{route('payment')}}">
                @csrf
                <div class="row">
                    <div class="col-lg-8 add_bottom_15">
                        <div class="form_title">
                            <h3><strong>1</strong>Your Details</h3>
                            <p>
                                Mussum ipsum cacilds, vidis litro abertis.
                            </p>
                        </div>
                        <div class="step">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>First name</label>
                                        <input type="text" class="form-control" id="firstname_booking"
                                               name="firstname_booking">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Last name</label>
                                        <input type="text" class="form-control" id="lastname_booking"
                                               name="lastname_booking">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Telephone</label>
                                        <input type="text" id="telephone_booking" name="telephone_booking"
                                               class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Alamat</label>
                                        <textarea type="email" id="email_booking" name="email_booking"
                                                  class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="policy">
                            <h4>Cancellation policy</h4>
                            <div class="form-group">
                                <label>
                                    <input type="checkbox" name="policy_terms" id="policy_terms">I accept terms and
                                    conditions and general policy.</label>
                            </div>
                            <button class="btn_1 green medium">Book now</button>
{{--                            <a href="confirmation_hotel.html" class="btn_1 green medium">Book now</a>--}}
                        </div>
                    </div>

                    <aside class="col-lg-4">
                        <div class="box_style_1">
                            <h3 class="inner">- Summary -</h3>
                            <table class="table table_summary">
                                <tbody>
                                <tr>
                                    <td>
                                        Check in
                                    </td>
                                    <td class="text-right">
                                        10 April 2015
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Check out
                                    </td>
                                    <td class="text-right">
                                        12 April 2015
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Rooms
                                    </td>
                                    <td class="text-right">
                                        1 double room
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Nights
                                    </td>
                                    <td class="text-right">
                                        2
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Adults
                                    </td>
                                    <td class="text-right">
                                        2
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Children
                                    </td>
                                    <td class="text-right">
                                        0
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Welcome bottle
                                    </td>
                                    <td class="text-right">
                                        $34
                                    </td>
                                </tr>
                                <tr class="total">
                                    <td>
                                        Total cost
                                    </td>
                                    <td class="text-right">
                                        $154
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <a class="btn_full_outline" href="{{route('tour')}}"><i class="icon-right"></i> Modify your
                                search</a>
                        </div>
                        <div class="box_style_4">
                            <i class="icon_set_1_icon-57"></i>
                            <h4>Need <span>Help?</span></h4>
                            <div class="row justify-content-md-center mt-3">
                                <div class="col-3">
                                    <a href="tel://11111" class="phone">
                                        <img class="img-thumbnail" style="max-width: 50px;"
                                             src="{{asset('images/icon-whatsapp.png')}}">
                                    </a>
                                </div>
                                <div class="col-3">
                                    <a href="tel://11111" class="phone">
                                        <img class="img-thumbnail" style="max-width: 50px;"
                                             src="{{asset('images/icon-gmail.png')}}">
                                    </a>
                                </div>
                            </div>
                            <small>Monday to Friday 9.00am - 7.30pm</small>
                        </div>
                    </aside>
                </div>
            </form>
        </div>
        <!--End container -->
    </main>
@endsection
@push('script')
    <script>
        $(function () {
            curForm.init();
        }), curForm = {
            init: function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-grey',
                    radioClass: 'iradio_square-grey'
                });
            }
        }
    </script>
@endpush
